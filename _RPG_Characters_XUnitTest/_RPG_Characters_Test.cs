using _RPG_Characters_;
using _RPG_Characters_.Character;
using _RPG_Characters_.Equipments;
using _RPG_Characters_.Exceptions;
using _RPG_Characters_.Heros;
using Xunit;

namespace _RPG_Characters_Test
{
    public class _RPG_Characters_Test
    {

        [Fact]
        public void Should_ThrowExpection_InvalidArmorExpection_Mage()
        {

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 10,
                ItemSlot = EquipmentType.Body,
                ArmourType = ArmorType.Cloth,
                Strength = 1

            };
            Mage mage = new Mage();
            Assert.Throws<InvalidArmorException>(() => mage.AddArmor(testPlateBody));
        }
        [Fact]
        public void Should_ThrowExpection_InvalidArmorExpection_Ranger()
        {

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 10,
                ItemSlot = EquipmentType.Body,
                ArmourType = ArmorType.Leather,
                Strength = 1

            };
            Ranger ranger = new Ranger();
            Assert.Throws<InvalidArmorException>(() => ranger.AddArmor(testPlateBody));
        }

        [Fact]
        public void Should_ThrowExpection_InvalidArmorExpection_Rogue()
        {

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 10,
                ItemSlot = EquipmentType.Body,
                ArmourType = ArmorType.Leather,
                Strength = 1

            };
            Rogue rogue = new Rogue();
            Assert.Throws<InvalidArmorException>(() => rogue.AddArmor(testPlateBody));
        }

        [Fact]
        public void Should_ThrowExpection_InvalidArmorExpection_Warrior()
        {

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = EquipmentType.Body,
                ArmourType = ArmorType.Cloth,
                Strength = 1

            };
            Warrior warrior = new Warrior();
            Assert.Throws<InvalidArmorException>(() => warrior.AddArmor(testPlateBody));
        }
        [Fact]
        public void Should_ThrowExpection_InvalidWeaponException_Mage()
        {

            Weapon AxeToLevel3 = new Weapon()
            {
                ItemName = "axe",
                ItemLevel = 10,
                ItemSlot = EquipmentType.Wepons,
                WeaponType = WeaponType.Wand,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            Mage mage = new Mage();
            Assert.Throws<InvalidWeaponException>(() => mage.AddWeaponItems(AxeToLevel3));
        }

        [Fact]
        public void Should_ThrowExpection_InvalidWeaponException_Ranger()
        {

            Weapon AxeToLevel3 = new Weapon()
            {
                ItemName = "bow",
                ItemLevel = 10,
                ItemSlot = EquipmentType.Wepons,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            Ranger ranger = new Ranger();
            Assert.Throws<InvalidWeaponException>(() => ranger.AddWeaponItems(AxeToLevel3));
        }

        [Fact]
        public void Should_ThrowExpection_InvalidWeaponException_Rogue()
        {

            Weapon AxeToLevel3 = new Weapon()
            {
                ItemName = "dagger",
                ItemLevel = 10,
                ItemSlot = EquipmentType.Wepons,
                WeaponType = WeaponType.Dagger,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            Rogue rouge = new Rogue();
            Assert.Throws<InvalidWeaponException>(() => rouge.AddWeaponItems(AxeToLevel3));
        }

        [Fact]
        public void Should_ThrowExpection_InvalidWeaponException_Warrior()
        {

            Weapon AxeToLevel3 = new Weapon()
            {
                ItemName = "axe",
                ItemLevel = 1,
                ItemSlot = EquipmentType.Wepons,
                WeaponType = WeaponType.Bow,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }

            };
            Warrior warrior = new Warrior();
            Assert.Throws<InvalidWeaponException>(() => warrior.AddWeaponItems(AxeToLevel3));
        }
        [Fact]
        public void Mage_Should_Be_Level_1()
        {
            Mage mage = new Mage();
            var expected = 1;
            var actual = mage.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_Should_Be_Level_1()
        {
            Ranger ranger = new Ranger();
            var expected = 1;
            var actual = ranger.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_Should_Be_Level_1()
        {
            Rogue rogue = new Rogue();
            var expected = 1;
            var actual = rogue.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_Should_Be_Level_1()
        {
            Warrior warrior = new Warrior();
            var expected = 1;
            var actual = warrior.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Level_Up_Mage()
        {
            Mage mage = new Mage();
            var expected = 2;
            mage.LevelUpMage();
            var actual = mage.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Level_Up_Ranger()
        {
            Ranger ranger = new Ranger();
            var expected = 2;
            ranger.LevelUpRanger();
            var actual = ranger.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Level_Up_Rogue()
        {
            Rogue rogue = new Rogue();
            var expected = 2;
            rogue.LevelUpRogue();
            var actual = rogue.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Level_Up_Warrior()
        {
            Warrior warrior = new Warrior();
            var expected = 2;
            warrior.LevelUpWarrior();
            var actual = warrior.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Throw_Proper_Attributes_Mage()
        {
            Mage mage = new Mage();
            double[] actual = new double[] { mage.Strength, mage.Dexterity, mage.Intelligence };
            double[] expected = new double[] { 1, 1, 8 };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Throw_Proper_Attributes_Ranger()
        {
            Ranger ranger = new Ranger();
            double[] actual = new double[] { ranger.Strength, ranger.Dexterity, ranger.Intelligence };
            double[] expected = new double[] { 1, 7, 1 };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Throw_Proper_Attributes_Rouge()
        {
            Rogue rogue = new Rogue();
            double[] actual = new double[] { rogue.Strength, rogue.Dexterity, rogue.Intelligence };
            double[] expected = new double[] { 2, 6, 1 };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Throw_Proper_Attributes_Warrior()
        {
            Warrior warrior = new Warrior();
            double[] actual = new double[] { warrior.Strength, warrior.Dexterity, warrior.Intelligence };
            double[] expected = new double[] { 5, 2, 1 };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Level_Up_Attribute_Mage()
        {
            Mage mage = new Mage();
            mage.LevelUpMage();
            double[] expected = new double[] { 2, 2, 13 };
            double[] actual = new double[] { mage.Strength, mage.Dexterity, mage.Intelligence };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Level_Up_Attribute_Ranger()
        {
            Ranger ranger = new Ranger();
            ranger.LevelUpRanger();
            double[] expected = new double[] { 2, 12, 2 };
            double[] actual = new double[] { ranger.Strength, ranger.Dexterity, ranger.Intelligence };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Level_Up_Attribute_Rogue()
        {
            Rogue rogue = new Rogue();
            rogue.LevelUpRogue();
            double[] expected = new double[] { 3, 10, 2 };
            double[] actual = new double[] { rogue.Strength, rogue.Dexterity, rogue.Intelligence };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_Level_Up_Attribute_Warrior()
        {
            Warrior warrior = new Warrior();
            warrior.LevelUpWarrior();
            double[] expected = new double[] { 8, 4, 2 };
            double[] actual = new double[] { warrior.Strength, warrior.Dexterity, warrior.Intelligence };
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Create_Weapon()
        {
            Weapon wapon = new Weapon();
        }

        [Fact]
        public void Create_Weapon_Axe()
        {
            Weapon axe = new Weapon()
            {
                WeaponAttributes = new WeaponAttributes(),
                WeaponType = WeaponType.Axe
            };
            Assert.NotNull(axe);

        }

        [Fact]
        public void Character_Should_Equip_A_Weapon_And_Armor_As_InvalidWeaponException_And_InvalidArmorException()
        {
            Weapon axe = new Weapon()
            {
                ItemName = "axe",
                ItemLevel = 1,
                ItemSlot = EquipmentType.Wepons,
                WeaponType = WeaponType.Staff,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor mail = new Armor()
            {
                ItemName = "Common Leather body armor",
                ItemLevel = 1,
                ItemSlot = EquipmentType.Body,
                ArmourType = ArmorType.Cloth,
                Strength = 1
            };
            Warrior warrior = new Warrior();
            Assert.Throws<InvalidWeaponException>(() => warrior.AddWeaponItems(axe));
            Assert.Throws<InvalidArmorException>(() => warrior.AddArmor(mail));
        }

        [Fact]
        public void IF_A_Character_Equips_A_Valid_Weapon_A_Success_Message_Should_Be_Returned()
        {
            Weapon weapon = new Weapon()
            {
                ItemName = "axe",
                ItemLevel = 1,
                ItemSlot = EquipmentType.Wepons,
                WeaponType = WeaponType.Hammer,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            Warrior warrior = new Warrior();
            var actual = warrior.AddWeaponItems(weapon);
            var expected = "New weapon equipped!";
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void If_A_Character_Equips_A_Valid_Armor_Piece_A_Success_Message_Should_Be_Returned()
        {
            Armor armor = new Armor()
            {
                ItemName = "Common Leather body armor",
                ItemLevel = 1,
                ItemSlot = EquipmentType.Body,
                ArmourType = ArmorType.Mail,
                Strength = 1
            };

            Warrior warrior = new Warrior();
            var actual = warrior.AddArmor(armor);
            var expected = "New armour equipped!";
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Should_Calculate_Damage_If_No_Weapon_Is_Equipped()
        {
            Warrior warrior = new Warrior();
            double expected = 1 * (1 + (5 / 100));
            double actual = warrior.CharacterDamage();
            Assert.Equal(Math.Round(expected), actual);
        }

        [Fact]
        public void Should_Calculate_Damage_With_ValidWeapon_Equipped()
        {
            Warrior warrior = new Warrior();
            Weapon weapon = new Weapon()
            {
                ItemName = "axe",
                ItemLevel = 1,
                ItemSlot = EquipmentType.Wepons,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            warrior.LevelUpWarrior();
            warrior.AddWeaponItems(weapon);
            double expected = (7 * 1.1) * (1 + (5 / 100));
            double actual = warrior.CharacterDamage();
            Assert.Equal(Math.Round(expected), actual);
        }

        [Fact]
        public void Should_Calculate_Damage_With_Valid_Weapon_And_Armor_Equipped()
        {

            Warrior warrior = new Warrior();
            //Take warrior level 1.
            Weapon weapon = new Weapon()
            //Equip axe.
            {
                ItemName = "axe",
                ItemLevel = 1,
                ItemSlot = EquipmentType.Wepons,
                WeaponType = WeaponType.Axe,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            warrior.AddWeaponItems(weapon);
            //Equip plate body armor.
            Armor armor = new Armor()
            {
                ItemName = "Common Leather body armor",
                ItemLevel = 1,
                ItemSlot = EquipmentType.Body,
                ArmourType = ArmorType.Plate,
                Strength = 1
            };
            warrior.AddArmor(armor);
            var actual = warrior.CharacterDamage();
            var expected = (7 * 1.1) * (1 + ((5 + 1) / 100));
            Assert.Equal(actual,expected);
         
        }





    }
}