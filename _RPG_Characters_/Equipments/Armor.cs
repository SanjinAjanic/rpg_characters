﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _RPG_Characters_.Equipments
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate,

    };
    public class Armor : Equipment
    {
        public ArmorType ArmourType { get; set; }

        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

    }
}

