﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _RPG_Characters_.Equipments
{
    public enum EquipmentType
    {
        Head,
        Body,
        Legs,
        Wepons
    };

    public abstract class Equipment
    {
        public string? ItemName { get; set; }
        public int ItemLevel { get; set; }
        //public  WeaponType WeaponType { get; set; }
        public EquipmentType ItemSlot { get; set; }
        public int LevelRequirement { get; set; }

        //public EquipmentType equipmentType { get; set; }   
        //public Equipment(EquipmentType etype)
        //{
        //    equipmentType = etype;

        //}

    }
}

