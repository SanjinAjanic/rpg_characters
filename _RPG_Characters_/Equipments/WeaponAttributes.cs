﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _RPG_Characters_
{
   public struct WeaponAttributes
    {
        public double AttackSpeed { get; set; }
        public double Damage { get; set; }

        public WeaponType WeaponType { get; set; }
        public double DPS
        {
            get { return Math.Round(Damage * AttackSpeed, 2);}
        }
    }
}
