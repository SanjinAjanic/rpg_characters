﻿
using _RPG_Characters_.Equipments;

namespace _RPG_Characters_
{
        public enum WeaponType
        {
            Axe,
            Bow, 
            Dagger, 
            Hammer, 
            Staff, 
            Sword, 
            Wand
        };

   public class Weapon: Equipment
    {
        public WeaponAttributes WeaponAttributes { get; set; }
        public WeaponType WeaponType { get; set; } 

        public WeaponType weaponType { get; set; }

    }
}

