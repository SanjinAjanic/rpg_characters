﻿using _RPG_Characters_.Equipments;
using _RPG_Characters_.Exceptions;

namespace _RPG_Characters_.Heros
{
    public class Ranger : Hero
    {
        /// <summary>
        /// Adds specific weapon to the Ranger hero
        /// </summary>
        /// <param name="weapon"></param>
        /// <exception cref="Exception"></exception>
        public override string AddWeaponItems(Weapon weapon)
        {
            if (weapon.WeaponType == WeaponType.Bow)
            {
                if (weapon.ItemLevel <= Level)
                {

                    base.AddWeaponItems(weapon);

                }
                else throw new InvalidWeaponException();
            }
            return "New weapon equipped!";

        }
        /// <summary>
        /// Adds the specific armor for the Ranger hero
        /// </summary>
        /// <param name="armor"></param>
        public override string AddArmor(Armor armor)
        {
            if (armor.ArmourType == ArmorType.Leather || armor.ArmourType == ArmorType.Mail)
                if (armor.ItemLevel <= Level)
                    base.AddArmor(armor);
                else throw new InvalidArmorException();
            else throw new InvalidArmorException();
            return "New armour equipped!";
        }

        public Ranger()
        {
            SetBaseLevelForAllHeroes(1, 7, 1);
            BasePrimaryAttributes.Add("Strength", 1);
            BasePrimaryAttributes.Add("Dexterity", 1);
            BasePrimaryAttributes.Add("Intelligence", 8);
            TotalPrimaryAttributes = BasePrimaryAttributes;
        }
        /// <summary>
        /// Level up the Ranger hero
        /// </summary>
        public void LevelUpRanger()
        {
            TotalPrimaryAttributes["Strength"] += 1;
            TotalPrimaryAttributes["Dexterity"] += 1;
            TotalPrimaryAttributes["Intelligence"] += 1;
            Strength += 1;
            Dexterity += 5;
            Intelligence += 1;
            LevelUp();
        }
    }
}
