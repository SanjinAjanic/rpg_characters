﻿using _RPG_Characters_.Equipments;
using _RPG_Characters_.Exceptions;

namespace _RPG_Characters_.Character
{
    public class Warrior : Hero
    {
        public object Exception { get; set; }

        /// <summary>
        /// Adds specific weapons to the Warrior hero
        /// </summary>
        /// <param name="weapon"></param>
        /// <exception cref="Exception"></exception>
        public override string AddWeaponItems(Weapon weapon)
        {

            if (weapon.WeaponType == WeaponType.Axe || weapon.WeaponType == WeaponType.Hammer || weapon.WeaponType == WeaponType.Sword)
            {

                if (weapon.ItemLevel <= Level)
                {

                    base.AddWeaponItems(weapon);

                }
                else throw new InvalidWeaponException();
            }

            else throw new InvalidWeaponException();
            return "New weapon equipped!";
        }
    /// <summary>
    /// Adds the specific armor for the Warrior hero
    /// </summary>
    /// <param name="armor"></param>
    public override string  AddArmor(Armor armor)
        {
            if (armor.ArmourType == ArmorType.Mail || armor.ArmourType == ArmorType.Plate)
                if (armor.ItemLevel <= Level)
                {
                    Strength += armor.Strength;
                    base.AddArmor(armor);
                }

                else throw new InvalidArmorException();
            else throw new InvalidArmorException();

         return "New armour equipped!";
        }
        public Warrior()
        {
            SetBaseLevelForAllHeroes(5, 2, 1);
            BasePrimaryAttributes.Add("Strength", 1);
            BasePrimaryAttributes.Add("Dexterity", 1);
            BasePrimaryAttributes.Add("Intelligence", 8);
            TotalPrimaryAttributes = BasePrimaryAttributes;
        }
        /// <summary>
        /// Level ups the Warrior hero
        /// </summary>
        public void LevelUpWarrior()
        {
            TotalPrimaryAttributes["Strength"] += 1;
            TotalPrimaryAttributes["Dexterity"] += 1;
            TotalPrimaryAttributes["Intelligence"] += 1;
            Strength += 3;
            Dexterity += 2;
            Intelligence += 1;
            LevelUp();
        }

        public object Throws<T>(Action value)
        {
            throw new NotImplementedException();
        }
    }
}
