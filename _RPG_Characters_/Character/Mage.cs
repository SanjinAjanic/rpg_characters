﻿using _RPG_Characters_.Equipments;
using _RPG_Characters_.Exceptions;

namespace _RPG_Characters_.Character
{
    public class Mage : Hero
    {
        /// <summary>
        /// Adds specific wepons for the Mage hero
        /// </summary>
        /// <param name="weapon"></param>
        /// <exception cref="Exception"></exception>
        public override string AddWeaponItems(Weapon weapon)
        {
            if (weapon.WeaponType == WeaponType.Staff || weapon.WeaponType == WeaponType.Wand)
            {
                if (weapon.ItemLevel <= Level)
                {

                    base.AddWeaponItems(weapon);

                }
                else throw new InvalidWeaponException();
            }

            return "New weapon equipped!";
        }
        /// <summary>
        /// Adds specific armor for the Mage hero
        /// </summary>
        /// <param name="armor"></param>
        public override string AddArmor(Armor armor)
        {
            if (armor.ArmourType == ArmorType.Cloth)
            {
                if (armor.ItemLevel <= Level)
                    base.AddArmor(armor);
                else throw new InvalidArmorException();
            }
            else throw new InvalidArmorException();
            return "New armour equipped!";
        }
        public Mage()
        {
            SetBaseLevelForAllHeroes(1, 1, 8);
            BasePrimaryAttributes.Add("Strength", 1);
            BasePrimaryAttributes.Add("Dexterity", 1);
            BasePrimaryAttributes.Add("Intelligence", 8);
            TotalPrimaryAttributes = BasePrimaryAttributes;

        }
        /// <summary>
        /// Levels up Mage hero
        /// </summary>
        public void LevelUpMage()
        {
            TotalPrimaryAttributes["Strength"] += 1;
            TotalPrimaryAttributes["Dexterity"] += 1;
            TotalPrimaryAttributes["Intelligence"] += 1;
            Strength += 1;
            Dexterity += 1;
            Intelligence += 5;
            LevelUp();
        }

    }

}
