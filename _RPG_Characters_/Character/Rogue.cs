﻿using _RPG_Characters_.Equipments;
using _RPG_Characters_.Exceptions;

namespace _RPG_Characters_.Character
{
    public class Rogue : Hero
    {
        /// <summary>
        /// Adds specific weapons to the Rouge Hero
        /// </summary>
        /// <param name="weapon"></param>
        /// <exception cref="Exception"></exception>
        public override string AddWeaponItems(Weapon weapon)
        {
            if (weapon.WeaponType == WeaponType.Dagger || weapon.WeaponType == WeaponType.Sword)
            {
                if (weapon.ItemLevel <= Level)
                {

                    base.AddWeaponItems(weapon);

                }
                else throw new InvalidWeaponException();
            }
            return "New weapon equipped!";

        }
        /// <summary>
        /// Adds the specific armor for the Rogue hero
        /// </summary>
        /// <param name="armor"></param>
        public override string AddArmor(Armor armor)
        {
            if (armor.ArmourType == ArmorType.Leather || armor.ArmourType == ArmorType.Mail)
                if (armor.ItemLevel <= Level)
                    base.AddArmor(armor);
                else throw new InvalidArmorException();
            else throw new InvalidArmorException();
            return "New armour equipped!";
        }
        public Rogue()
        {
            SetBaseLevelForAllHeroes(2, 6, 1);
            BasePrimaryAttributes.Add("Strength", 1);
            BasePrimaryAttributes.Add("Dexterity", 1);
            BasePrimaryAttributes.Add("Intelligence", 8);
            TotalPrimaryAttributes = BasePrimaryAttributes;
        }
        /// <summary>
        /// Level up the Rouge Hero
        /// </summary>
        public void LevelUpRogue()
        {
            TotalPrimaryAttributes["Strength"] += 1;
            TotalPrimaryAttributes["Dexterity"] += 1;
            TotalPrimaryAttributes["Intelligence"] += 1;
            Strength += 1;
            Dexterity += 4;
            Intelligence += 1;
            LevelUp();
        }
    }
}
