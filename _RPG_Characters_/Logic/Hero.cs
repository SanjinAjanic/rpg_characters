﻿using _RPG_Characters_.Equipments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _RPG_Characters_
{

    public class Hero
    {
        public List<Equipment> equipment = new List<Equipment>();

        public Dictionary<string, double> BasePrimaryAttributes = new Dictionary<string, double>(); // ökar när du levlar upp

        public Dictionary<string, double> TotalPrimaryAttributes = new Dictionary<string, double>(); // ökar med armor eller wepon

        public Dictionary<string, Equipment> Gear = new Dictionary<string, Equipment>();
        public StringBuilder stringBuilder { get; set; } = new StringBuilder(); 


        public double Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Level { get; set; }
        public string? Name { get; set; }
        public int Dps { get; set; }
        public int Aps { get; set; }

        /// <summary>
        /// Displays the characters stats.
        /// </summary>
        public void characterStatsDisplay()
        {

        stringBuilder.Append($"Name: {Name} \n Level: {Level} \n Strength: {Strength} \n Dexterity: {Dexterity} \n Intelligence: {Intelligence} \n Damage: {CharacterDamage()}");
            Console.WriteLine(stringBuilder);
        }
        /// <summary>
        /// Adds weapon to Characters
        /// </summary>
        /// <param name="weapon"></param>
        public virtual string AddWeaponItems(Weapon weapon)
        {
            Gear.Add(weapon.ItemName, weapon);
            return "New weapon equipped!";
        }
        /// <summary>
        /// Adds armor to Character
        /// </summary>
        /// <param name="armor"></param>
        public virtual string AddArmor(Armor armor)
        {
            Gear.Add(armor.ItemName, armor);

            return "New armour equipped!";
        }

        /// <summary>
        /// Levels up all heroes +
        /// </summary>
        protected void LevelUp()
        {
            this.Level += 1;

        }
        /// <summary>
        /// Calculates the Characters damage whit weapon and without weapon
        /// </summary>
        /// <returns></returns>
        public virtual double CharacterDamage()
        {

            var weaponGear = (Weapon)Gear.FirstOrDefault(x => x.Value is Weapon).Value;
            if (weaponGear is null)
            {
                return Math.Round(1 + Strength / 100);
            }
            return Math.Round(weaponGear.WeaponAttributes.DPS * (1 + Strength / 100));

        }

        /// <summary>
        /// Sets the base level for all characters
        /// </summary>
        /// <param name="str"></param>
        /// <param name="dex"></param>
        /// <param name="intelligence"></param>
        public void SetBaseLevelForAllHeroes(int str, int dex, int intelligence)
        {
            this.Level = 1;
            this.Strength = str;
            this.Dexterity = dex;
            this.Intelligence = intelligence;
        }
        /// <summary>
        /// sets custom name of characters
        /// </summary>
        /// <param name="name"></param>
        public void SetName(string name)
        {
            Name = name;
        }
    }
   
}

